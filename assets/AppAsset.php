<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'public/css/reset.css',
        'public/css/style.css',
        'public/css/slider.css',
        'public/css/skin.css',
        'http://fonts.googleapis.com/css?family=Cabin+Sketch:400,700',
    ];
    public $js = [
        'public/js/jquery-1.7.min.js',
        'public/js/jquery.easing.1.3.js',
        'public/js/tms-0.4.1.js',
        'public/js/jquery.fancybox-1.3.4.pack.js',
        'public/js/jquery.jcarousel.min.js',
        'public/js/tabs.js',
        'public/js/main.js',
    ];
    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}

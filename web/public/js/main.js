
$().ready(function () {
    $('.slider')
        ._TMS({
           show: 0,
           pauseOnHover: false,
           prevBu: false,
           nextBu: false,
           playBu: false,
           duration: 700,
           preset: 'fade',
           pagination: '.pags',
           pagNums: false,
           slideshow: 7000,
           numStatus: false,
           banners: false, // fromLeft, fromRight, fromTop, fromBottom
           waitBannerAnimation: false,
           progressBar: false
        });
    jQuery('#mycarousel')
        .jcarousel({
            horisontal: true,
            wrap: 'circular',
            scroll: 1,
            easing: 'easeInOutBack',
            animation: 1200
        });
});

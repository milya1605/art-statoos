<?php

/* @var $this yii\web\View */
use yii\bootstrap\Html;

$this->title = 'Тату салон в Брянске: фото татуировок и нанесение по доступным ценам';
?>
<div class="gallery-block">
    <h1>Нанесение татуировки в тату-салоне <span style="font-size:38px;font-style:normal;" class="stattoos">StattooS</span></h1>
</div>
<div class="gallery-block">
    <?= $this->render('inc/main_gallery')?>
</div>
<div class="page1-row1 pad-1">
    <div class="col-1">
        <div id="block" class="h3 p2">Категории услуг:</div>
        <?= $this->render('inc/category');?>
    </div>
    <div class="col-2"><br>
        <p>С примерами наших работ можно ознакомится на странице "Портфолио".</p>
        <h2>Сделать татуировку по доступной цене в Брянске</h2>
        <p>Вам нужно нанести татуировку - обращайтесь к нам!</p>
        <br>
        <div id="block" class="h2 p2">Коротко о нас:</div>
        <p class="p1">Мы занимаемся нанесение татуировок на протяжении нескольких лет.</p>
        <p>На странице "О нас" вы можете ознакомится с полной биографией нашей тату-студии.</p>
        <a href="[[~6]]" class="link-1 link-1-pad bot-1">Подробнее</a>
        <div class="clear"></div>
        <div id="block" class="h3">Мы в соцсети:</div>
        <div class="soc-icons-1"> <a href="#"><?= Html::img('@web/public/images/vk.png');?></a> <a href="#"><?= Html::img('@web/public/images/ok.png');?></a> <a href="#"><?= Html::img('@web/public/images/facebook.png');?></a> </div>
    </div>
    <div class="col-3">
        <div id="block" class="h2 p2">Контакты</div>
        <div class="adr">
            <p class="p3">
                <strong>Телефон:</strong> <span class="clr-1"><a class="clr-1" href="tel:+79206062010">+7 (920) 606-20-10</a></span><br>
                <strong>E-mail:</strong> <a href="mailto:info@art-stattoos.ru" class="clr-1">info@art-stattoos.ru</a></p>
            <p class="clr-1">г. Брянск,<br>
                ул. Красноармейская, 62/2</p>
        </div>
    </div>
</div>

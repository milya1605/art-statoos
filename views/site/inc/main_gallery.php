<?php
use yii\bootstrap\Html;
?>
<ul id="mycarousel" class="jcarousel-skin-tango gallery-photo">
    <li><a href="#"><?= Html::img('@web/public/images/gallery-1.jpg');?></a><a href="#"><?= Html::img('@web/public/images/gallery-6.jpg');?></a></li>
    <li><a href="#"><?= Html::img('@web/public/images/gallery-2.jpg');?></a><a href="#"><?= Html::img('@web/public/images/gallery-7.jpg');?></a></li>
    <li><a href="#"><?= Html::img('@web/public/images/gallery-3.jpg');?></a><a href="#"><?= Html::img('@web/public/images/gallery-8.jpg');?></a></li>
    <li><a href="#"><?= Html::img('@web/public/images/gallery-4.jpg');?></a><a href="#"><?= Html::img('@web/public/images/gallery-2.jpg');?></a></li>
    <li><a href="#"><?= Html::img('@web/public/images/gallery-5.jpg');?></a><a href="#"><?= Html::img('@web/public/images/gallery-7.jpg');?></a></li>
</ul>
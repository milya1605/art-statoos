<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<body>
<!--==============================header=================================-->
<div class="header">
    <div>

        <div class="nav">
            <nav>
                <ul class="menu">
                    <li class="current li-none"><a>Главная</a></li>
                    <li><a href="[[~6]]">О нас</a></li>
                    <li><a href="[[~7]]">Портфолио</a></li>
                    <li class="li-left li-none"><a href="[[~8]]">Услуги</a></li>
                    <li><a href="[[~9]]">Отзывы</a></li>
                    <li><a href="[[~10]]">Контакты</a></li>
                </ul>
            </nav>
        </div>

        <header>
            <div id="logo-img"><a><?= Html::img('@web/public/images/logo.png', ['alt'=>'тату салон Арт-Статус', 'title'=>'нанесение тату в салоне Арт-Статус']);?></a></div>
        </header>

        <div id="slide">
            <div class="slider">
                <ul class="items">
                    <li><?= Html::img('@web/public/images/slide-1.jpg');?></li>
                    <li><?= Html::img('@web/public/images/slide-2.jpg');?></li>
                    <li><?= Html::img('@web/public/images/slide-3.jpg');?></li>
                    <li><?= Html::img('@web/public/images/slide-4.jpg');?></li>
                    <li><?= Html::img('@web/public/images/slide-5.jpg');?></li>
                </ul>
            </div>
            <ul class="pags">
                <li><a href="#"><strong>0</strong>1</a></li>
                <li><a href="#"><strong>0</strong>2</a></li>
                <li><a href="#"><strong>0</strong>3</a></li>
                <li><a href="#"><strong>0</strong>4</a></li>
                <li><a href="#"><strong>0</strong>5</a></li>
            </ul>
        </div>
    </div>

</div>

<!--==============================content================================-->
<section id="content">
    <?= $content;?>
</section>
<!--==============================footer=================================-->
<footer> <span><strong>© 2012-2017 тату салон StattooS</strong></span> </footer>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
